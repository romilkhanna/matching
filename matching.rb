def match(ground, quantified)
	ground_relation = ""
	ground_facts = []
	ground_objects = []

	# Format: relation(fact(object))
	words = ground.gsub(/\(|\)|,/, " ").split(" ").each { |s| s.strip }

	if words.count == 2		# Brother(Romil)
		ground_facts << words.shift
		ground_objects << words.shift
	elsif words.count == 3	# Sibling(Jim, Alex)
		ground_relation << words.shift
		ground_objects << words.shift
		ground_objects << words.shift
	else words.count > 3	# Loves(Romil, Dog(Rocky)) or more facts within second argument
		ground_relation << words.shift
		ground_objects << words.shift
		ground_facts << words.shift until words.count == 1
		ground_objects << words.shift
	end

	quantified_relation = ""
	quantified_facts = []
	quantified_objects = []

	words = quantified.gsub(/\(|\)|,/, " ").split(" ").each { |s| s.strip }

	if words.count == 2		# Brother(x)
		quantified_facts << words.shift
		quantified_objects << words.shift

		if ground_facts == quantified_facts
			line = "Match found for #{ground_facts[0]}(): "
			line << "#{quantified_objects[0]} = #{ground_objects[0]}"
			puts line
			return true
		end
	else					# Sibling(x, y)
		quantified_relation << words.shift
		quantified_objects << words.shift while words.any?

		if ground_relation.eql?(quantified_relation)
			if quantified_objects[0] == quantified_objects[1]
				line = "Failed to match, could not find distinct bindings."
				line << " Same object: #{quantified_objects[0]} was provided for the quantified formula."
				puts line
				return false
			end

			line = "Match found for #{ground_relation}(): "
			line << "#{quantified_objects[0]} = #{ground_objects[0]} and "
			line << "#{quantified_objects[1]} = "
			
			ground_facts.each { |s|
				line << "#{s}("
			} if ground_facts.count > 0

			line << "#{ground_objects[1]}"
			ground_facts.count.times { line << ")" } if ground_facts.count > 0
			puts line
			return true
		end
	end

	line = "No match found for: #{quantified} in #{ground}."
	if quantified_relation != ground_relation
		line << " Relation doesn't match."
	else
		line << " Fact doesn't match."
	end
	puts line
	return false
end

tests = [
	{ :ground => "Brother(Romil)", :quantified => "Brother(x)", :result => true },
	{ :ground => "Brother(Romil)", :quantified => "Bro(x)", :result => false },

	{ :ground => "Sibling(Jim, Alex)", :quantified => "Sibling(x, y)", :result => true },
	{ :ground => "Sibling(Jim, Alex)", :quantified => "Siblings(x, y)", :result => false },

	{ :ground => "Loves(Romil, Dog(Rocky))", :quantified => "Loves(x, y)", :result => true },
	{ :ground => "Loves(Romil, Dog(Rocky))", :quantified => "L(x, y)", :result => false },

	{ :ground => "Loves(Romil, Husky(Dog(Rocky)))", :quantified => "Loves(x, y)", :result => true },
	{ :ground => "Loves(Romil, Husky(Dog(Rocky)))", :quantified => "Love(x, y)", :result => false },

	{ :ground => "Younger(Romil, Husky(Puppy(Dog(Rocky))))", :quantified => "Younger(x, y)", :result => true },
	{ :ground => "Younger(Romil, Husky(Puppy(Dog(Rocky))))", :quantified => "Young(x, y)", :result => false },
]

tests.each { |test|
	match(test[:ground], test[:quantified])
}